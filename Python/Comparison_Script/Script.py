# def readFile(fileName):
#         fileObj = open(fileName, "r") #opens the file in read mode
#         words = fileObj.read().splitlines() #puts the file into an array
#         fileObj.close()
#         return words
# Customers = readFile("E:\\Work\\VSLearning\\Python\\Comparison_Script\\Customers.out")
# Mapping = readFile("E:\\Work\\VSLearning\\Python\\Comparison_Script\\Mapping.out")
# print (Customers)
# print (Mapping)
# for i in Customers:
#      print (i[1])
#============================
from csv import reader

def fileReader(fileName):
    with open(fileName, 'r') as read_obj:
        # pass the file object to reader() to get the reader object
        csv_reader = reader(read_obj)
        # Pass reader object to list() to get a list of lists
        list_of_rows = list(csv_reader)
        #print(list_of_rows)
        return list_of_rows

mapping_list = fileReader("E:\\Work\\VSLearning\\Python\\Comparison_Script\\Mapping.out")
customer_list = fileReader("E:\\Work\\VSLearning\\Python\\Comparison_Script\\Customers_mod.out")

mapping_list_len = len(mapping_list)
customer_list_len = len(customer_list)

#print (mapping_list_len)
#print (customer_list_len)

i = 0
j = 0

output_list = open("E:\\Work\\VSLearning\\Python\\Comparison_Script\\Output.out", "w")

while i < customer_list_len:
    #print ("===============")
    #print (str(i) +" Number")
    #print (customer_list[i][1])
    while j < mapping_list_len:
        #print (mapping_list[j])
        if customer_list[i][0] == mapping_list[j][0]:
            output =(customer_list[i][0] + "," + customer_list[i][1] + "," + mapping_list[j][1])
            output_list.write(customer_list[i][0] + "," + customer_list[i][1] + "," + mapping_list[j][1] + "\n")
            break
        j += 1
    j = 0
    i += 1

output_list.close()
