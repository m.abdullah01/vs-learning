import time

with open('FILENAME') as f:
        names = f.readlines()[:10]
        names_set = set(names)

def in_names():
    ret = []
    for i in range(100):
        ret.append(str(i) in names)
    return ret

def in_names_set():
    ret = []
    for i in range(100):
        ret.append(str(i) in names_set)
    return ret

print ("Running in_names")
s = time.time()
in_names()
print ("Time Taken:", time.time() - s)

print ("Running in_names_set")
s = time.time()
in_names_set()
print ("Time Taken:", time.time() - s)
